<?php
date_default_timezone_set('Europe/Paris');

try{
  // Creation de la SQLite
  $file_db = new PDO('sqlite:biblioFilm.sqlite3');
  //Gerer le niveau des erreurs rapportées
  $file_db->setAttribute(PDO::ATTR_ERRMODE,
                          PDO::ERRMODE_EXCEPTION);

$file_db->exec("CREATE TABLE IF NOT EXISTS Realisateur (
  IdReal INTEGER PRIMARY KEY,
  NomReal TEXT,
  Prenom TEXT)");

$file_db->exec("CREATE TABLE IF NOT EXISTS Genre(
  IdGenre INTEGER PRIMARY KEY,
  NomGenre TEXT)");


  $file_db->exec("CREATE TABLE IF NOT EXISTS Film (
    IdFilm INTEGER PRIMARY KEY,
    Titre TEXT,
    Annee TEXT,
    Description TEXT,
    IdReal INTEGER,
    IdGenre INTEGER,
    FOREIGN KEY (IdReal) REFERENCES Realisateur(IdReal),
    FOREIGN KEY (IdGenre) REFERENCES Genre (IdGenre))");
    $file_db = null;
  }
  catch(PDOException $e) {
    echo $e->getMessage();
  }
?>
